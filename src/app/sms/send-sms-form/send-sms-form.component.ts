import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SmsService } from '../../core/sms/sms.service';
import { Observable } from 'rxjs/Observable';
import { SmsResponse } from '../../core/sms/sms.models';
import { BitlyService } from '../../core/bitly.service';
import { mergeMap } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-send-sms-form',
  templateUrl: './send-sms-form.component.html',
  styleUrls: ['./send-sms-form.component.scss']
})
export class SendSmsFormComponent implements OnInit {

  smsForm: FormGroup;
  get toControl(): FormControl { return this.smsForm.get('to') as FormControl; }
  get messageControl(): FormControl { return this.smsForm.get('message') as FormControl; }
  alert: { okay: boolean; message: string };

  constructor(private formBuilder: FormBuilder, private smsService: SmsService, private bitlyService: BitlyService) {
    this.buildForm();
  }

  ngOnInit() {
    this.alert = {
      okay: false,
      message: ''
    };
  }

  private buildForm() {
    this.smsForm = this.formBuilder.group({
      to: ['', Validators.required],
      message: ['', [
        Validators.required,
        Validators.maxLength(459)
      ]]
    });
  }

  submit() {
    if (this.smsForm.invalid) {
      return;
    }

    let to = this.toControl.value;
    let message = this.messageControl.value;
    let sms$: Observable<SmsResponse>;
    let smsSubscription: Subscription;

    // parse message
    sms$ = this.bitlyService.parseAll(message).pipe(
      // and then send sms
      mergeMap((parsedMessage: string) => this.smsService.send(parsedMessage, to))
    );

    this.smsForm.disable();
    this.alert.message = '';

    smsSubscription = sms$.subscribe(
      (smsResponse: SmsResponse) => {
        console.log('sms response ', smsResponse)
        this.smsForm.reset();
        this.alert.okay = true;
        this.alert.message = 'Your message was sent!';
      },
      (error) => {
        console.log('error in sending request', error)
        this.smsForm.enable();
        this.alert.okay = false;
        this.alert.message = 'Unable to send due to an error :(';
      },
      () => smsSubscription.unsubscribe()
    );
  }


}
