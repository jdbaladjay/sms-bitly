import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendSmsFormComponent } from './send-sms-form.component';

describe('SendSmsFormComponent', () => {
  let component: SendSmsFormComponent;
  let fixture: ComponentFixture<SendSmsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendSmsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendSmsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
