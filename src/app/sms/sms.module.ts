import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SmsRoutingModule } from './sms-routing.module';
import { SmsComponent } from './sms.component';
import { SendSmsFormComponent } from './send-sms-form/send-sms-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SmsRoutingModule
  ],
  declarations: [SmsComponent, SendSmsFormComponent]
})
export class SmsModule { }
