import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Sms, SmsResponse } from './sms.models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BitlyService } from '../bitly.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class SmsService {

  constructor(private http: HttpClient, private bitlyService: BitlyService) { }

  send(message: string, to: string): Observable<SmsResponse> {
    let authData = btoa(environment.smsKey + ':' + environment.smsSecret);

    const options = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + authData
      })
    };

    let sms: Sms = {
      message: message,
      to: to
    };

    console.log('sms ', sms);

    return this.http.post<SmsResponse>(environment.smsApiUrl, sms, options);
  }

}
