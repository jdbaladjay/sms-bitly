import { Injectable } from '@angular/core';
import { from } from 'rxjs/observable/from';
import { of } from 'rxjs/observable/of';
import { map, concatMap, reduce } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class BitlyService {

  private urlRegex = new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/);

  constructor(private http: HttpClient) { }

  shorten(urlText: string): Observable<string> {
    let options = {
      params: {
        access_token: environment.bitlyAccessToken,
        longUrl: urlText
      }
    }

    return this.http.get<string>(environment.bitlyApiUrl, options).pipe(
      map(x => x['data'].url)
    );
  }

  parseAll(message:string): Observable<string> {
    // Break message into words
    return from(message.split(' ')).pipe(
      // replace with short link
      concatMap((word: string) => {
        if (this.urlRegex.test(word)) {
          return this.shorten(word);

        } else {
          return of(word);
        }
      }),

      // restore the message
      reduce((acc, val) => acc + ' ' + val)
    );
  }

}
