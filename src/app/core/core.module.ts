import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { SmsService } from './sms/sms.service';
import { BitlyService } from './bitly.service';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule
  ],
  declarations: [],
  providers: [
    SmsService,
    BitlyService
  ]
})
export class CoreModule { }
