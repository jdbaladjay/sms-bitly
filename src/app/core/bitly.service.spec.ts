import { TestBed, inject } from '@angular/core/testing';

import { BitlyService } from './bitly.service';

describe('BitlyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BitlyService]
    });
  });

  it('should be created', inject([BitlyService], (service: BitlyService) => {
    expect(service).toBeTruthy();
  }));
});
